from gym.envs.registration import register

register(
    id='wumpus4-v0',
    entry_point='gym_wumpus4.envs:WumpusWorld4',
    kwargs={'map_name' : '4x4'},
    max_episode_steps=100,
    reward_threshold=0.78, # optimum = .8196
),



    