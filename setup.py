from setuptools import setup

setup(name='gym_wumpus4',
      version='0.0.1',
      author='E.R. De Jager',
      install_requires=['gym', 'numpy']  # And any other dependencies wumpus needs
)  